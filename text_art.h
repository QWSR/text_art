#ifndef __H__TEXT_ART__H__
#define __H__TEXT_ART__H__

#define EXPANDED_FACE_LENGTH 16
#define EXPANDED_FACE_HEIGHT 9
#define NUM_FACE 27

#define EXPANDED_FACE_LENGTH_STR 17

#define WHITESPACE_OFFSET 0
#define UPPERCASE_OFFSET 1

struct __letter {
	char face;
	char expanded_face[EXPANDED_FACE_HEIGHT][EXPANDED_FACE_LENGTH_STR];
};

void write_expanded_face(char *dest, char *src);

#endif
