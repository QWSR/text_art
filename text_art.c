#include "stdlib.h"
#include "string.h"
#include "ctype.h"

#include "text_art.h"

struct __letter letter_face[NUM_FACE] = {
	{' ',
		{
			"________________",
			"________________",
			"________________",
			"________________",
			"________________",
			"________________",
			"________________",
			"________________",
			"________________"
		}
	},

	{'A', 
		{
			"_______AA_______",
			"______AAAA______",
			"____ AAAAAA_____",
			"____AAA__AAA____",
			"___AAAAAAAAAA___",
			"__AAAAAAAAAAAA__",
			"_AAA________AAA_",
			"AAA__________AAA",
			"________________"
		}
	},

	{'B', 
		{
			"_BBBBBBBBBBBB___",
			"_BBB_______BBB__",
			"_BBB_______BBB__",
			"_BBBBBBBBBBBB___",
			"_BBBBBBBBBBBB___",
			"_BBB_______BBB__",
			"_BBB_______BBB__",
			"_BBBBBBBBBBBB___",
			"________________"
		}
	},

	{'C', 
		{
			"_CCCCCCCCCCCCC__",
			"_CCCCCCCCCCCCC__",
			"_CCCC___________",
			"_CCCC___________",
			"_CCCC___________",
			"_CCCC___________",
			"_CCCCCCCCCCCCC__",
			"_CCCCCCCCCCCCC__",
			"________________"
		}
	},

	{'D', 
		{
			"_DDDDDDDDDD_____",
			"_DDDDDDDDDDD____",
			"_DDD      DDD___",
			"_DDD       DDD__",
			"_DDD       DDD__",
			"_DDD      DDD___",
			"_DDDDDDDDDDD____",
			"_DDDDDDDDDD_____",
			"________________"
		}
	},

	{'E', 
		{
			"_EEEEEEEEEEEEEE_",
			"_EEEEEEEEEEEEEE_",
			"_EEE____________",
			"_EEEEEEEEEEEEEE_",
			"_EEEEEEEEEEEEEE_",
			"_EEE____________",
			"_EEEEEEEEEEEEEE_",
			"_EEEEEEEEEEEEEE_",
			"________________"
		}
	},

	{'F', 
		{
			"_FFFFFFFFFFFFFF_",
			"_FFFFFFFFFFFFFF_",
			"_FFF____________",
			"_FFF____________",
			"_FFFFFFFFFF_____",
			"_FFFFFFFFFF_____",
			"_FFF____________",
			"_FFF____________",
			"________________"
		}
	},

	{'G', 
		{
			"_GGGGGGGGGGGGGG_",
			"_GGGGGGGGGGGGGG_",
			"_GGG____________",
			"_GGG_____GGGGGG_",
			"_GGG_____GGGGGG_",
			"_GGG________GGG_",
			"_GGGGGGGGGGGGGG_",
			"_GGGGGGGGGGGGGG_",
			"________________"
		}
	},

	{'H', 
		{
			"_HHH_______HHH__",
			"_HHH_______HHH__",
			"_HHH_______HHH__",
			"_HHHHHHHHHHHHH__",
			"_HHHHHHHHHHHHH__",
			"_HHH_______HHH__",
			"_HHH_______HHH__",
			"_HHH_______HHH__",
			"________________"
		}
	},

	{'I', 
		{
			"___IIIIIIIIII___",
			"___IIIIIIIIII___",
			"______IIII______",
			"______IIII______",
			"______IIII______",
			"______IIII______",
			"___IIIIIIIIII___",
			"___IIIIIIIIII___",
			"________________"
		}
	},

	{'J', 
		{
			"__JJJJJJJJJJJ___",
			"__JJJJJJJJJJJ___",
			"______JJJJ______",
			"______JJJJ______",
			"_JJJ__JJJJ______",
			"_JJJ__JJJJ______",
			"__JJJJJJJ_______",
			"___JJJJJ________",
			"________________"
		}
	},

	{'K', 
		{
			"____KKK___KKK___",
			"____KKK__KKK____",
			"____KKK_KKK_____",
			"____KKKKKK______",
			"____KKK_KKK_____",
			"____KKK__KKK____",
			"____KKK___KKK___",
			"____KKK____KKK__",
			"________________"
		}
	},

	{'L', 
		{
			"__LLL___________",
			"__LLL___________",
			"__LLL___________",
			"__LLL___________",
			"__LLL___________",
			"__LLL___________",
			"__LLLLLLLLLLLL__",
			"__LLLLLLLLLLLL__",
			"________________"
		}
	},

	{'M', 
		{
			"_MM___________MM",
			"_MMM_________MMM",
			"_MMMM_______MMMM",
			"_MM_MM_____MM_MM",
			"_MM__MM___MM__MM",
			"_MM__MMMMMM___MM",
			"_MM___MMMM____MM",
			"_MM____MM_____MM",
			"________________"
		}
	},

	{'N', 
		{
			"_NNNN_______NN__",
			"_NN_NN______NN__",
			"_NN__NN_____NN__",
			"_NN___NN____NN__",
			"_NN____NN___NN__",
			"_NN_____NN__NN__",
			"_NN______NN_NN__",
			"_NN_______NNNN__",
			"________________"
		}
	},

	{'O', 
		{
			"_OOOOOOOOOOOOOO_",
			"_OOOOOOOOOOOOOO_",
			"_OO__________OO_",
			"_OO__________OO_",
			"_OO__________OO_",
			"_OO__________OO_",
			"_OOOOOOOOOOOOOO_",
			"_OOOOOOOOOOOOOO_",
			"________________"
		}
	},

	{'P', 
		{
			"__PPPPPPPPPPPP__",
			"__PPPPPPPPPPPP__",
			"__PPP______PPP__",
			"__PPP______PPP__",
			"__PPPPPPPPPPPP__",
			"__PPPPPPPPPPPP__",
			"__PPP___________",
			"__PPP___________",
			"________________"
		}
	},

	{'Q', 
		{
			"_QQQQQQQQQQQQ___",
			"_QQQQQQQQQQQQ___",
			"_QQ________QQ___",
			"_QQ________QQ___",
			"_QQ_______QQQ___",
			"_QQQQQQQQQQQQQ__",
			"_QQQQQQQQQQQQQQ_",
			"_____________QQQ",
			"________________"
		}
	},

	{'R', 
		{
			"___RRRRRRR______",
			"___RRR__RRR_____",
			"___RRR__RRR_____",
			"___RRR_RRR______",
			"___RRRRRR_______",
			"___RRR_RRR______",
			"___RRR__RRR_____",
			"___RRR___RRR____",
			"________________"
		}
	},

	{'S', 
		{
			"__SSSSSSSSSSSS__",
			"__SSSSSSSSSSSS__",
			"__SSS___________",
			"__SSSSSSSSSSSS__",
			"__SSSSSSSSSSSS__",
			"___________SSS__",
			"__SSSSSSSSSSSS__",
			"__SSSSSSSSSSSS__",
			"________________"
		}
	},

	{'T', 
		{
			"__TTTTTTTTTTTT__",
			"__TTTTTTTTTTTT__",
			"______TTTT______",
			"______TTTT______",
			"______TTTT______",
			"______TTTT______",
			"______TTTT______",
			"______TTTT______",
			"________________"
		}
	},

	{'U', 
		{
			"_UUU________UUU_",
			"_UUU________UUU_",
			"_UUU________UUU_",
			"_UUU________UUU_",
			"_UUU________UUU_",
			"_UUU________UUU_",
			"_UUUUUUUUUUUUUU_",
			"_UUUUUUUUUUUUUU_",
			"________________"
		}
	},

	{'V', 
		{
			"VVV__________VVV",
			"_VVV________VVV_",
			"__VVV______VVV__",
			"___VVV____VVV___",
			"____VVV__VVV____",
			"_____VVVVVV_____",
			"______VVVV______",
			"_______VV_______",
			"________________"
		}
	},

	{'W', 
		{
			"W______________W",
			"WW____________WW",
			"_WW____WW____WW_",
			"__WW__WWWW__WW__",
			"___WWWW__WWWW___",
			"____WWW__WWW____",
			"_____WW__WW_____",
			"______W__W______",
			"________________"
		}
	},

	{'X', 
		{
			"XXX__________XXX",
			"__XXX______XXX__",
			"____XXX__XXX____",
			"______XXXX______",
			"______XXXX______",
			"____XXX__XXX____",
			"__XXX______XXX__",
			"XXX__________XXX",
			"________________"
		}
	},

	{'Y', 
		{
			"YYY__________YYY",
			"__YYY______YYY__",
			"____YYY__YYY____",
			"______YYYY______",
			"_______YY_______",
			"_______YY_______",
			"_______YY_______",
			"_______YY_______",
			"________________"
		}
	},

	{'Z', 
		{
			"ZZZZZZZZZZZZZZZZ",
			"___________ZZZ__",
			"_________ZZZ____",
			"_______ZZZ______",
			"_____ZZZ________",
			"___ZZZ__________",
			"_ZZZ____________",
			"ZZZZZZZZZZZZZZZZ",
			"________________"
		}
	}
};

struct __letter* search_letter(char face) {
	if(isspace(face)) {
		return letter_face;
	} else if(isupper(face)) {
		return letter_face + UPPERCASE_OFFSET + face - 'A';
	} else {
		return NULL;
	}
}

char* write_line(char *dest, char *begin, char *end) {
	char *index = begin;

	if(begin != end) {
		for(unsigned int i=0; i<EXPANDED_FACE_HEIGHT; ++i) {
			while(index < end) {
				struct __letter *char_result = search_letter(*index);
				
				if(char_result!=NULL) {
					memcpy(dest, (char_result->expanded_face)[i], EXPANDED_FACE_LENGTH);
					dest += EXPANDED_FACE_LENGTH;
		
					++index;
				}

			}

			*dest = '\n';
			++dest;

			index = begin;
		}
	} else {
		memset(dest, '\n', EXPANDED_FACE_HEIGHT);
		dest += EXPANDED_FACE_HEIGHT;
	}

	return dest;	
}

//dest must have atleast 128x the length of src
void write_expanded_face(char *dest, char *src) {
	char *line_begin = src;
	char *line_end = src;

	while(*src != '\0') {
		if(*src == '\n') {
			dest = write_line(dest, line_begin, line_end);
			++line_end;		
			line_begin = line_end;
		} else {
			++line_end;
		}

		++src;
	}

	*dest = '\0';
}
